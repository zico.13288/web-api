from bottle import run, view, route, request, redirect
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError, InvalidRequestError

from db import db, Ticket
from sqlalchemy.orm import sessionmaker

engine = create_engine('postgresql://postgres:1@localhost:5432/')
Session = sessionmaker(bind=engine)
s = Session()


@route('/')
@view("index")
def index():
    return


@route('/add', method="GET")
def add():
    number = request.query['number']
    date = request.query['date']
    time = request.query['time']
    ticket = Ticket(number=number, date=date, time=time)
    try:
        if number.isdigit() and len(number) == 3:
            s.add(ticket)
            s.commit()
            return redirect("/")
        else:
            return "Введите номер из трех символов состоящий из чисел "
    except IntegrityError:
        return "Такой билет уже есть в базе"
    except InvalidRequestError:
        return "Такой билет уже есть в базе"


if __name__ == '__main__':
    run(host="localhost", port=8080)
