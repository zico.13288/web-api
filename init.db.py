from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from db import db

engine = create_engine('postgresql://postgres:1@localhost:5432/', echo=True)
db.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
s = Session()
