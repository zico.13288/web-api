from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String


db = declarative_base()


class Ticket(db):
    __tablename__ = "tickets"

    number = Column(Integer, primary_key=True)
    date = Column(String(20), unique=True)
    time = Column(String(20), unique=True)

    def __init__(self, number, date, time):
        self.number = number
        self.date = date
        self.time = time
